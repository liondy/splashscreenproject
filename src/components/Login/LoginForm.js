import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    TextInput,
    TouchableOpacity,
    Text,
    Image
} from 'react-native'

export default class LoginForm extends Component {
    state={
        email:'',
        password:'',
        hidePassword: true
    };
    handleEmail = (text) => {
        this.setState({email: text})
    }
    handlePassword = (text) => {
        this.setState({password: text})
    }
    managePasswordVisibility=()=>{
        this.setState({hidePassword: !this.state.hidePassword});
    }
    login = (email,password) => {
        alert('My Email: '+email+'\nMy Password: '+ password)
    }
    render() {
        return (
            <View style={styles.container}>
                <TextInput 
                    style={styles.input}
                    keyboardType='email-address'
                    autoCapitalize='none'
                    onChangeText={this.handleEmail}
                    placeholder="Enter your username"
                    placeholderTextColor='rgba(0,0,0,0.4)'
                    returnKeyType='next'
                    onSubmitEditing={() => this.passwordInput.focus()}
                />
                <TextInput 
                    style={styles.input}
                    secureTextEntry = {this.state.hidePassword}
                    onChangeText={this.handlePassword}
                    placeholder="Enter your password"
                    placeholderTextColor='rgba(0,0,0,0.4)'
                    returnKeyType='go'
                    ref={(input) => this.passwordInput = input}
                    onSubmitEditing={()=>this.login(this.state.email,this.state.password)}
                />
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.passwordVisibility}
                    onPress={this.managePasswordVisibility}>
                    <Image 
                        source={(this.state.hidePassword)?require('../../images/hide.png'):require('../../images/show.png')}
                        style={styles.btnPasswordVisibility}
                    />
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.buttonLoginContainer}
                    onPress={
                        ()=>this.login(this.state.email,this.state.password)
                    }>
                    <Text style={styles.button}>LOGIN</Text>
                </TouchableOpacity>
                <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity onPress={this.props.action}>
                        <Text style={styles.noAccount}>I don't have an account</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        padding: 20
    },
    input:{
        height: 50,
        backgroundColor: 'rgba(255,255,255,0.7)',
        marginBottom: 15,
        color: '#000',
        paddingHorizontal: 10,
        borderRadius: 5
    },
    buttonLoginContainer:{
        borderRadius: 5,
        backgroundColor: '#95a5a6',
        paddingVertical: 20
    },
    button:{
        textAlign: 'center',
        color: '#FFF',
        fontWeight: '700'
    },
    passwordVisibility:{
        position: 'absolute',
        top: 85,
        right: 30,
        width: 35,
        height: 50,
    },
    btnPasswordVisibility:{
        resizeMode: 'contain',
        height: '100%',
        width: '100%',
        opacity: .5
    },
    noAccount:{
        marginTop: 10,
        paddingVertical: 10,
        color: '#FFF',
        fontWeight: '700',
        opacity: .8
    }
});
