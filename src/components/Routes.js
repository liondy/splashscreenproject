import React, { Component } from 'react'

import HomeContent from './Home/HomeScreen';
import LoginContent from './Login/LoginForm';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';

class HomeScreen extends React.Component{
    render(){
        return(
            <HomeContent/>
        )
    }
}

class LoginScreen extends React.Component{
    render(){
        return(
            <LoginContent/>
        )
    }
}

const MainNavigator = createStackNavigator({
    Home : HomeScreen,
    Login : LoginScreen
},{
    initialRouteName : 'Home'
});

const App = createAppContainer(MainNavigator);

export default App;
