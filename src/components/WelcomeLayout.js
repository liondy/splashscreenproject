import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Image,
    Text
} from 'react-native'
import Routes from './Routes'

export default class Login extends Component {
    state={
        welcome: true
    };
    goToHome=()=>{
        this.setState({welcome:true});
    };
    goToLogin=()=>{
        this.setState({welcome:false});
    };
    goBack=()=>{
        this.setState({welcome:true});
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image
                        source={require('../images/bebras.png')}
                        style={styles.logo}
                    />
                    <Text style={styles.title}>Welcome to Bebras Challenge Indonesia</Text>
                </View>
                <View style={styles.formContainer}>
                    <Routes/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#7f8c8d'
    },
    logoContainer:{
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1
    },
    logo:{
        width: 250,
        height: 250
    },
    title:{
        color: '#FFF',
        marginTop: 10,
        textAlign: 'center',
        opacity: 0.9
    }
});