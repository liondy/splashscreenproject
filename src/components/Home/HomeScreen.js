import React, { Component } from 'react'
import {
    View,
    TouchableOpacity,
    StyleSheet,
    Text
} from 'react-native'

export default class HomeScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.button2Container}>
                    <Text style={styles.button2}>GET STARTED</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.buttonContainer}
                    onPress={this.props.action}>
                    <Text style={styles.button}>I ALREADY HAVE AN ACCOUNT</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        padding: 20,
    },
    buttonContainer:{
        borderRadius: 5,
        backgroundColor: '#95a5a6',
        paddingVertical: 20,
        marginTop: 15
    },
    button2Container:{
        borderRadius: 5,
        backgroundColor: '#FFF',
        paddingVertical: 20,
        marginTop: 15
    },
    button:{
        textAlign: 'center',
        color: '#FFF',
        fontWeight: '700'
    },
    button2:{
        textAlign: 'center',
        color: '#95a5a6',
        fontWeight: '700'
    },
});