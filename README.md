# SplashScreenProject

A project that is developed with React-Native (still learning tho)

## Instalasi
Tutorial instalasi React-Native : https://facebook.github.io/react-native/docs/getting-started <br> + Watchman :)

## Run the project

`git clone https://gitlab.com/liondy/splashscreenproject.git` <br>

For Android : <br>

Windows
1. `npx react-native run-android`

Linux
1. `npm start`
2. `npx react-native run-android`

## UI App
1. Splash Screen <br>
![image](/uploads/ea49ca9960c721796b9bff9a4ec75619/image.png) <br>

2. Welcome Screen <br>
![image](/uploads/8a8c47f24f2d8f0bd006c5ac5dc9e410/image.png) <br>

3. Login Screen <br>
![image](/uploads/2e66cff385e4367a4c0c9b83d2e30a32/image.png) <br>

4. Password Hidden State <br>
![image](/uploads/533a8285625ec46dc9f6c2a0d0e9c9b5/image.png) <br>

5. Password Visible State <br>
![image](/uploads/8f03476dd9a84c82b7a64c7ca02aa37c/image.png) <br>

6. Login State (Getting user input) <br>
![image](/uploads/74845381cf82f1c6ee1f636eedc9d5f7/image.png)