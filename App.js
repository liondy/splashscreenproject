/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment,useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
} from 'react-native';

import SplashScreen from 'react-native-splash-screen'
import WelcomeLayout from './src/components/WelcomeLayout'

const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  },[]);
  return(
    <Fragment>
        <View style={styles.sectionContainer}>
            <StatusBar barStyle= "light-content"/>
            <WelcomeLayout/>
        </View>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
      flex: 1,
  }
});

export default App;
